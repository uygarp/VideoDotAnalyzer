import sys
from os import path
import cv2
import numpy as np
from observer import Observer, Observable
from video_reader import VideoReader
from frame_matcher import FrameMatcher
from presenter import Presenter

# PARAMS
# video_file, red_image_file, white_image_file, ref_frame_no
# RETURNS list 
param_number = 4
#video_path = '1_50_0_0.R324753.mp4'
#ref_no = 30
#red_image_file_name = 'red.jpg'
#white_image_file_name = 'white.jpg'

red_offset = 1
white_offset = 1
#match_method = cv2.TM_SQDIFF_NORMED
#match_method = cv2.TM_CCOEFF_NORMED
match_method = cv2.TM_CCORR_NORMED
match_rect = [1300, 850, 1600, 1000] # x,y,x+w,y+h
    
def get_params():
    if len(sys.argv) != param_number + 1:
        print("Missing or too-many parameters...")
        print("Usage:")
        print("\tCOMMAND [video-file] [red_file] [white-file] [frame-no]")
        exit(1)
    video_path = sys.argv[1]
    red_image_file_name = sys.argv[2]
    white_image_file_name = sys.argv[3]
    ref_no_str = sys.argv[4]
    if not path.exists(video_path):
        print "Cannot find video file"
        exit(1)
    if not path.exists(red_image_file_name):
        print "Cannot find red image file"
        exit(1)
    if not path.exists(white_image_file_name):
        print "Cannot find video file"
        exit(1)
    if not ref_no_str.isdigit():
        print("Frame number is not valid..")
        exit(1)
    return video_path, red_image_file_name, white_image_file_name, ref_no_str

if __name__ == "__main__":
    video_path, red_image_file_name, white_image_file_name, ref_no_str = get_params()
    ref_no = int(ref_no_str)
    
    video_reader = VideoReader(video_path)
    video_reader.set_default_rect(match_rect)
    ref_image, duration = video_reader.get_nth_frame(ref_no - 1)
    images_to_compare = [cv2.imread(red_image_file_name),
                        cv2.imread(white_image_file_name)]
    frame_matcher = FrameMatcher(ref_image,
                                images_to_compare,
                                match_method,
                                video_reader.get_max_frame_no(),
                                [red_offset, white_offset])

    video_reader.register(frame_matcher)
    video_reader.runAll()

    presenter = Presenter(frame_matcher.get_results(), video_reader.get_frame_duration)
    presenter.display()
    video_reader.endAll()

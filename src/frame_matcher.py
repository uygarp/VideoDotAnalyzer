import sys
import cv2
import numpy as np
from observer import Observer

method_no = 1

class FrameMatcher(Observer):
    images_to_compare = []
    def __init__(self, ref_image, images_to_compare, match_method, max_frame_no, offset_list = []):
        self.images_to_compare = images_to_compare
        self.match_method = match_method
        if len(images_to_compare) == len(offset_list):
            self.offset_list = offset_list
        else:
            self.offset_list = [1] * len(images_to_compare)
        self.ref_positions = []
        self.results = []        
        for frame in images_to_compare:
            position = self.get_position_of_image(ref_image, frame)
            self.ref_positions.append(position)
        self.max_frame_no = max_frame_no
    
    # OBSERVER
    def notified(self, data):
        frame_index, frame, duration = data#[0]
        #frame = data[1]
        current_result = []
        index = 0
        progress = int(100 * frame_index / self.max_frame_no)
        sys.stdout.write("Analysing... %d%%   \r" % (progress))
        sys.stdout.flush()
        for image in self.images_to_compare:
            ref_pos = self.ref_positions[index]
            if method_no == 2:
                x, y = self.find_image(frame, image)
                found = (x != 0 and y != 0)
            else:
                found = self.is_frame_contains(frame, image, ref_pos, self.offset_list[index])
            current_result.append(found)
            index += 1
        #print("frame_index:{} found:{}".format(frame_index, current_result))
        self.results.append(current_result)
        if frame_index == self.max_frame_no - 1:
            print("Finished.........\n")    

    def get_results(self):
        return self.results
    
    def is_frame_contains(self, frame, image_to_search, ref_pos, offset = 1):
        foundX, foundY = self.get_position_of_image(frame, image_to_search)
        #if foundX != 0 and foundY != 0:
        #    print("[{}, {}]".format(foundX, foundY))
        return ((foundX <= (ref_pos[0] + offset) and foundX >= (ref_pos[0] - offset))
                and (foundY <= (ref_pos[1] + offset) and foundY >= (ref_pos[1] - offset))) 
    
    def get_position_of_image(self, frame, image_to_search):
        res = cv2.matchTemplate(image_to_search, frame, self.match_method)
        threshold = 0.8
        loc = np.where( res >= threshold)
        if len(loc[0]) != 0 and len(loc[1]) != 0:
            mnVal,mxVal,mnLoc,mxLoc = cv2.minMaxLoc(res)
            if (self.match_method == cv2.TM_SQDIFF or self.match_method == cv2.TM_SQDIFF_NORMED):
                foundX,foundY = mnLoc
            else:
                foundX,foundY = mxLoc        
            return (foundX, foundY)
        return (0, 0)
    
    def find_image(self, im, tpl):
        im = np.atleast_3d(im)
        tpl = np.atleast_3d(tpl)
        H, W, D = im.shape[:3]
        h, w = tpl.shape[:2]

        # Integral image and template sum per channel
        sat = im.cumsum(1).cumsum(0)
        tplsum = np.array([tpl[:, :, i].sum() for i in range(D)])

        # Calculate lookup table for all the possible windows
        iA, iB, iC, iD = sat[:-h, :-w], sat[:-h, w:], sat[h:, :-w], sat[h:, w:] 
        lookup = iD - iB - iC + iA
        # Possible matches
        possible_match = np.where(np.logical_and.reduce([lookup[..., i] == tplsum[i] for i in range(D)]))

        # Find exact match
        for y, x in zip(*possible_match):
            if np.all(im[y+1:y+h+1, x+1:x+w+1] == tpl):
                return (x+1, y+1)

        return (0,0)

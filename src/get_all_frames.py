import sys
from os import path
import cv2
from video_reader import VideoReader

# PARAMS
# video_file, ref_frame_no
# RETURNS frame for red and white image 
param_number = 2
#video_path = '1_50_0_0.R324753.mp4'
#ref_no = 297

match_rect = [1300, 850, 1600, 1000]

def get_params():
    if len(sys.argv) != param_number + 1:
        print("Missing or too-many parameters...")
        print("Usage:")
        print("\tCOMMAND [video-file] [destination]")
        exit(1)
    
    video_path = sys.argv[1]
    dest = sys.argv[2]

    if not path.exists(video_path):
        print "Cannot find video file"
        exit(1)

    if not path.exists(dest):
        print("Destination is not valid..")
        exit(1)
    return video_path, dest

if __name__ == "__main__":
    video_path, dest = get_params()
    
    video_reader = VideoReader(video_path)
    max_frame_no = video_reader.get_max_frame_no()
    video_reader.set_default_rect(match_rect)
    print(max_frame_no)
    #exit(1)
    
    for frame_index in range(max_frame_no):
        progress = int(100 * frame_index / max_frame_no)
        sys.stdout.write("Getting frames ... %d%%   \r" % (progress))
        sys.stdout.flush()
        
        frame, duration = video_reader.get_nth_frame(frame_index)
        #duration = video_reader.get_frame_duration(frame_index)
        duration_int = int(round(duration))
        
        base_name = os.path.splitext(os.path.basename(video_path))[0]
        dest_full_path = os.path.join(dest, base_name)

        image_file_name = '{}_{}_{}.jpg'.format(dest_full_path, frame_index, duration_int) 
                
        cv2.imwrite(image_file_name, frame)
    
    print("Finished ........           ")


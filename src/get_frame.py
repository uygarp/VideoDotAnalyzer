import sys
import os
from os import path
import cv2
from video_reader import VideoReader

# PARAMS
# video_file, ref_frame_no
# RETURNS frame for red and white image 
param_number = 2
#video_path = '1_50_0_0.R324753.mp4'
#ref_no = 297

match_rect = [1300, 850, 1600, 1000]

def get_params():
    if len(sys.argv) != param_number + 1:
        print("Missing or too-many parameters...")
        print("Usage:")
        print("\tCOMMAND [video-file] [frame-no]")
        exit(1)
    
    video_path = sys.argv[1]
    ref_no_str = sys.argv[2]

    if not path.exists(video_path):
        print "Cannot find video file"
        exit(1)

    if not ref_no_str.isdigit():
        print("Frame number is not valid..")
        exit(1)
    return video_path, ref_no_str

if __name__ == "__main__":
    video_path, ref_no_str = get_params()
    ref_no = int(ref_no_str)

    video_reader = VideoReader(video_path)
    video_reader.set_default_rect(match_rect)
    ref_image, duration = video_reader.get_nth_frame(ref_no)
    #duration = video_reader.get_frame_duration(ref_no) + 0.5
    duration_int = int(round(duration))
    
    base_name = os.path.splitext(os.path.basename(video_path))[0]

    ref_image_file_name = '{}_{}_{}.jpg'.format(base_name, ref_no, duration_int) 
    cv2.imwrite(ref_image_file_name, ref_image)
    print("--> {}".format(ref_image_file_name))
    #print("DURATION:\t{}".format(duration_int))


from abc import ABCMeta, abstractmethod

class Observable:
    __metaclass__ = ABCMeta
    observers = []
    def register(self, observer):
        if observer not in self.observers:
            self.observers.append(observer)
    def deregister(self, observer):
        self.observers.remove(observer)
    def _update(self, data):
        for observer in self.observers:
            observer.notified(data)
            
class Observer:
    __metaclass__ = ABCMeta
    @abstractmethod
    def notified(self, data):
        pass
 

import numpy as np

apply_patch = True
    
class Presenter(object):
    def __init__(self, results, duration_func):
        self.results = results
        self.duration_func = duration_func
        
    def display(self):
        len_results = len(self.results)
        len_result = len(self.results[0])
        on_list = [[], []]
        off_list = [[], []]
        prev_result = self.results[0]
        for ii in range(0, len_results):
            for jj in range(len_result):
                found = self.results[ii][jj]
                if prev_result[jj] != found:
                    if found:
                        on_list[jj].append(ii)
                    else:
                        off_list[jj].append(ii)
                prev_result[jj] = found

        #CUSTOMISED Part    
        red_on_list = on_list[0]
        red_off_list = off_list[0]
        white_on_list = on_list[1]
        white_off_list = off_list[1]
        min_len = min(len(red_on_list), len(red_off_list), len(white_on_list), len(white_off_list))
        if min_len == 0:
            print("No Result...")
            exit(1)
        
        # PATCH
        if apply_patch:
            min_red_on_off = 5
            min_wht_on_off = 5
            for ii in range(min_len):
                if ii > 0:
                    if red_on_list[ii] < (red_off_list[ii - 1] + min_red_on_off):
                        print("Patch applied for red index({}).".format(ii))
                        del red_on_list[ii]
                        del red_off_list[ii - 1]
                    if white_off_list[ii] < (white_on_list[ii - 1] + min_wht_on_off):
                        print("Patch applied for white index({}).".format(ii))
                        del white_off_list[ii]
                        del white_on_list[ii - 1]
            min_len = min(len(red_on_list), len(red_off_list), len(white_on_list), len(white_off_list))
            if min_len == 0:
                print("No Result...")
                exit(1)
        # PATCH finished

        print("RED-ON\tRED-OFF\tWHT-OFF\tWHT-ON\t\tRESULT(frame)")
        diff = []
        for ii in range(min_len):
            diff.append(white_off_list[ii] - red_on_list[ii])
            print("{}\t{}\t{}\t{}\t\t{}".format(
                                        red_on_list[ii],
                                        red_off_list[ii],
                                        white_off_list[ii],
                                        white_on_list[ii],
                                        diff[ii]
                                        ))
        average = round(np.average(diff), 2)
        print("\t\t\tAVE(fr):\t{}\n".format(average))
        
        print("RED-ON\tRED-OFF\tWHT-OFF\tWHT-ON\t\tRESULT(ms)")
        diff_ms = []
        for ii in range(0, min_len):
            dur_red_on = int(round(self.duration_func(red_on_list[ii])))
            dur_red_off = int(round(self.duration_func(red_off_list[ii])))
            dur_wht_on = int(round(self.duration_func(white_on_list[ii])))
            dur_wht_off = int(round(self.duration_func(white_off_list[ii])))
            diff_ms.append(dur_wht_off - dur_red_on)
            print("{}\t{}\t{}\t{}\t\t{}".format(
                                        dur_red_on,
                                        dur_red_off,
                                        dur_wht_on,
                                        dur_wht_on,
                                        diff_ms[ii]
                                        ))
        average = int(round(np.average(diff_ms)))
        print("\t\t\tAVE(ms):\t{}\n".format(average))

import cv2
from observer import Observable

class VideoReader(Observable):
    def __init__(self, video_path):
        self.capture = cv2.VideoCapture(video_path)
        self.capture.set(cv2.CAP_PROP_POS_AVI_RATIO, 1)
        ret,_ = self.capture.read()
        prop_pos_frames = int(self.capture.get(cv2.CAP_PROP_POS_FRAMES))
        self.max_frame_no = prop_pos_frames + 1 if ret else prop_pos_frames
        self.capture.set(cv2.CAP_PROP_POS_AVI_RATIO, 0)
        self.def_rect = [0, 0,
                        int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH)),
                        int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))]
    
    def runAll(self):
        for frame_index in range(self.max_frame_no):
            frame, duration = self.get_nth_frame(frame_index)
            self._update((frame_index, frame, duration))
    
    def endAll(self):
        self.capture.release()
        cv2.destroyAllWindows()
        
    def __read_frame(self):
        ret, frame = self.capture.read()
        if ret:
            return frame
        else:
            print("Error occurred while reading video file.")
            self.endAll()
            exit(1)
        
    def get_frame_duration(self, frame_index):
        self.capture.set(cv2.CAP_PROP_POS_FRAMES, frame_index)
        self.__read_frame()
        return self.capture.get(cv2.CAP_PROP_POS_MSEC)
        
    def get_max_frame_no(self):
        return self.max_frame_no

    def set_default_rect(self, def_rect):
        self.def_rect = def_rect
        
    def get_nth_frame(self, frame_index):
        if self.max_frame_no <= frame_index:
            print("Frame number is too much")
            exit(1)
        self.capture.set(cv2.CAP_PROP_POS_FRAMES, frame_index)
        frame = self.__read_frame()
        frame = frame[self.def_rect[1]:self.def_rect[3], self.def_rect[0]:self.def_rect[2]]
        duration = self.capture.get(cv2.CAP_PROP_POS_MSEC)
        return frame, duration
